from os import link
import requests
from bs4 import BeautifulSoup
import datetime
import time

import pymysql

pwd = ''

def create():
    db = pymysql.connect(host='localhost',
                     user='root',
                     password=pwd,
                     database='test')
 
    
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()

     # 使用 execute() 方法执行 SQL，如果表存在则删除
    cursor.execute("DROP TABLE IF EXISTS spyders")
    
    # 使用预处理语句创建表
    sql = """CREATE TABLE spyders (
            id int(10) PRIMARY KEY AUTO_INCREMENT,
            link  VARCHAR(2000),
            notice VARCHAR(2000)
             )"""
             #subtitle  VARCHAR(255)
             #website  VARCHAR(255)
    cursor.execute(sql)                 

    db.close



def CON(value1,value2):
    db = pymysql.connect(host='localhost',
                     user='root',
                     password=pwd,
                     database='test')
    
 
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    
    sql = f'insert into spyders(link,notice) values("{value1}","{value2}")'

    print(value1)
    
    try:
         # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
        print('插入数据成功')
    except:
        # 发生错误时回滚
        db.rollback()
        print('插入数据失败')
    # 关闭数据库连接
    db.close()


def spider():#爬虫
    url = 'http://www2.scut.edu.cn/gzic/'
    resp = requests.get(url)
    resp.encoding = 'gbk2312'
    soup = BeautifulSoup(resp.text,'html.parser')

    #数据整理
    list1=[]#第一次扒下来的数据整理的大列表（不用管）
    for k in soup.select('div[class ="li-tt fr"]'):
        list1.append(str(k.a))


    LINK=[]#活动网页链接列表

    NOTICE=[]#活动通知列表

    num=0
    for j in list1:
        LINK.append(j.split('<a href="')[-1].split('</a>')[0].split('">')[0])
        NOTICE.append(j.split('<a href="')[-1].split('</a>')[0].split('">')[1])
        
        CON(LINK[num],NOTICE[num])
        #print(LINK[num],NOTICE[num])
        num+=1
    


#定时爬
def time_ti(h=12, m=50):#参数为每天定时爬虫时间
    while True:
        now = datetime.datetime.now()
        # print(now.hour, now.minute)
        if now.hour == h and now.minute == m:
            spider()



create()
spider()
