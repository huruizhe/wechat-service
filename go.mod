module main

go 1.13

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-ini/ini v1.66.4 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	gorm.io/driver/sqlite v1.3.1 // indirect
)
